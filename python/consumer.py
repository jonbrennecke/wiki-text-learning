#!/usr/bin/env python

"""
Consumes a Kafka queue
"""

from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
import argparse
import sys

if __name__ == '__main__':

    # Get kafka broker (eg. localhost:9092) and topic from arguments
    
    parser = argparse.ArgumentParser(description='Consumes a Kafka topic.')
    parser.add_argument('-t', '--topic', metavar='TOPIC', type=str, required=True,
                       help='Kafka topic')
    parser.add_argument('-b', '--broker', metavar='BROKER', type=str, required=True,
                       help='Kafka broker url')
    args = parser.parse_args()
    topic = args.topic
    broker = args.broker

    # Configure and start Spark
    
    conf = (SparkConf()
         .setMaster("local[4]") # use 4 cores on the local machine
         .setAppName("Kafka Consumer")
         .set("spark.executor.memory", "1g"))
    sc = SparkContext(conf=conf)
    ssc = StreamingContext(sc, 2)

    kafkaStream = KafkaUtils.createDirectStream(ssc, [topic], {"metadata.broker.list": broker})

    i = 0

    def log_rdd(rdd):
        global i
        i += rdd.count()
        print i


    kafkaStream.foreachRDD(log_rdd)
    # .transform(storeOffsetRanges)

    ssc.start()
    ssc.awaitTermination()