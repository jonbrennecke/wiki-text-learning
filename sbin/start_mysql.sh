#!/usr/bin/env bash

# Starts MySQL

green="\x1B[32m"
red="\x1B[31m"
normal="\x1B[0m"

printf "Starting MySQL... "

mysql.server start > /dev/null

if [ $? -eq 0 ]; then
    printf "%b" "${green}✓\n${normal}"
else
    printf "%b" "${red}✖\n${normal}"
fi