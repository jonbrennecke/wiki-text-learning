#!/usr/bin/env bash

# Starts the Kafka Wiki Producer

green="\x1B[32m"
red="\x1B[31m"
normal="\x1B[0m"

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
conf_dir=${dir}/../conf

printf "Starting Wiki Producer... "

wiki_kafka_producer --config ${conf_dir}/config.json &> /dev/null &

if [ $? -eq 0 ]; then
    printf "%b" "${green}✓\n${normal}"
else
    printf "%b" "${red}✖\n${normal}"
fi