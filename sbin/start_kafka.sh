#!/usr/bin/env bash

# Starts Kafka

green="\x1B[32m"
red="\x1B[31m"
normal="\x1B[0m"

printf "Starting Kafka... "

kafka_home=$(brew --prefix kafka)
kafka_etc=/usr/local/etc/kafka

${kafka_home}/bin/kafka-server-start.sh ${kafka_etc}/server.properties  &> /dev/null &

# ${kafka_home}/bin/kafka-topics.sh --create --zookeeper localhost:2181 \
# 	--replication-factor 1 --partitions 1 --topic wiki-pages

if [ $? -eq 0 ]; then
    printf "%b" "${green}✓\n${normal}"
else
    printf "%b" "${red}✖\n${normal}"
fi