#!/usr/bin/env bash

# Starts Spark Consumer

spark_home=$(brew --prefix apache-spark)
pyspark=${spark_home}/bin/pyspark

# export these vars for Hadoop or else Spark will complain that 
# it's "unable to load native-hadoop library for your platform"
# Nope, still complains
# hadoop_home=$(brew --prefix hadoop)
# export HADOOP_COMMON_LIB_NATIVE_DIR=$hadoop_home/lib/native
# export HADOOP_OPTS="-Djava.library.path=$hadoop_home/lib"

green="\x1B[32m"
red="\x1B[31m"
normal="\x1B[0m"

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
project_home=${dir}/..

# locate the kafka streaming jar file
groupId=org.apache.spark
artifactId=spark-streaming-kafka-assembly_2.10
version="1.6.0"
m2_spark_home=/.m2/repository/org/apache/spark/${artifactId}
pathname="${m2_spark_home}/${version}/${artifactId}-${version}.jar"
streamingjar=$HOME/$pathname;

# locate the python script
py_dir=${project_home}/python
python_src=${py_dir}/consumer.py

# locate the config file
conf_dir=${project_home}/conf/
conf_json_file=${conf_dir}/config.json

# get the host, port and topic from the JSON config file
config=($(node -e "
	var json = require(\"${conf_json_file}\");
	console.log(json.kafka.topic);
	console.log(json.kafka.host);
	console.log(json.kafka.port);"))

topic=${config[0]}
host=${config[1]}
port=${config[2]}

kafka_broker="${host}:${port}"

printf "Starting Spark Consumer... "

${spark_home}/bin/spark-submit --jars \
      ${streamingjar} \
      ${python_src} \
      --broker ${kafka_broker} --topic ${topic}

if [ $? -eq 0 ]; then
    printf "%b" "${green}✓\n${normal}"
else
    printf "%b" "${red}✖\n${normal}"
fi