#!/usr/bin/env bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

${dir}/start_mysql.sh
${dir}/start_zookeeper.sh
${dir}/start_kafka.sh
${dir}/start_wiki_producer.sh
${dir}/start_spark_consumer.sh

